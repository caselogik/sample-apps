package loan

class LoanAppWI implements WorkItem {
    String key;
    int riskScore;
}

//process
process ('approve_loan') {

    name = 'Approve Loan'

    swimlanes {
        swimlane('risk')
        swimlane('finance')
        swimlane('comms')

    }

    //nodes
    nodes {

        start {
            workItem = LoanAppWI
        }

        task('evaluateRisk') {
            name = 'Evaluate Risk'
            swimlane = 'risk'
            execution {
                by = human
                url = '/evaluate-risk'
            }

        }

        orGate('or_1'){
            rule {
                if(_workData.riskScore > 5) _goVia('risky')
                else _goVia('notRisky')
            }
        }


        andGate('and_1')

        task('makePayment') {
            name = 'Make Payment'
            swimlane = 'finance'
            execution {
                by = system
                when = immediately
                url = 'http://localhost:4300/api/make-payment'
            }
        }

        task('sendDocs') {
            name = 'Send Documents'
            swimlane = 'comms'
            execution {
                by = human
                url = '/send-docs'
            }
        }

        task('emailRejection') {
            name = 'Email Rejection'
            swimlane = 'comms'
            execution {
                by = system
                when = immediately
                url = 'http://localhost:8081/email-rejection'
            }
        }

    }

    //flow
    flow {
        start >> N('evaluateRisk') >> N('or_1')-['notRisky'] >> N('and_1')
        N('or_1')-['risky'] >> N('emailRejection') >> end
        N('and_1') >> N('makePayment') >> end
        N('and_1') >> N('sendDocs') >> end
    }

}

// npm install express --save
// npm install body-parser --save
// npm install mongoose --save

var express = require('express');  
var path = require("path");   
var bodyParser = require('body-parser');  
var mongo = require("mongoose");  
  
var db = mongo.connect("mongodb://localhost:27017/loan-approval", function(err, response){  
   if(err){ console.log( err); }  
   else{ console.log('Connected to ' + db, ' + ', response); }  
});  
  
   
var app = express()  
app.use(bodyParser());  
app.use(bodyParser.json({limit:'5mb'}));   
app.use(bodyParser.urlencoded({extended:true}));  
   
  
app.use(function (req, res, next) {        
     res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');    
     res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');    
     res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');      
     res.setHeader('Access-Control-Allow-Credentials', true);       
     next();  
 });  
  
var Schema = mongo.Schema;  
  
var LoanApplicationSchema = new Schema({      
  applicantName: { type: String },   
  applicantAge: { type: Number },    
  loanAmount: { type: Number },  
  riskScore: { type: Number },
  rejectionSent: { type: Boolean },
  docContent: { type: String },
  paymentSent: { type: Boolean }
},{ 
  versionKey: false 
});  
   
  
var model = mongo.model('applications', LoanApplicationSchema, 'applications');  
  
app.post("/api/applications",function(req,res){   
    var mod = new model(req.body)
    mod.save(function(err,data){  
      if(err){  
        res.send(err);                
      }  
      else{        
        res.send({"status": "OK", "_id": data._id});  
      }  
    });  
})  
 
app.put("/api/applications",function(req,res){   

  model.findByIdAndUpdate(req.body._id, { riskScore: req.body.riskScore },  
    function(err,data) {  
    if (err) {  
      res.send(err);       
    }  
    else{        
      res.send({"status": "OK"});  
    }  
  });   
})

app.delete("/api/applications",function(req,res){      
    model.remove({ _id: req.body._id }, function(err) {    
      if(err){    
        res.send(err);    
      }    
      else{      
        res.send({data:"Record has been Deleted..!!"});               
      }    
    });    
})  
  
app.get("/api/applications",function(req,res){  
  model.find({},function(err,data){  
    if(err){  
      res.send(err);  
    }  
    else{             
      res.send(data);  
    }  
  });  
})  

app.get("/api/make-payment",function(req,res){  
  var tid = req.query.taskId
  model.findByIdAndUpdate(req.body._id, { paymentSent: true },  
    function(err,data) {  
    if (err) {  
      res.send(err);       
    }  
    else{        
      res.send({ _type: "completed", taskId: tid });
    }  
  });   

}) 
  
app.listen(4300, function () {  
 console.log('Example app listening on port 4300');  
})  
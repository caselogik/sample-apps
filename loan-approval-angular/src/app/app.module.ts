import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TaskManagementComponent } from './task-management/task-management.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { NewCaseComponent } from './task-management/new-case/new-case.component';
import { EvaluateRiskComponent } from './evaluate-risk/evaluate-risk.component';
import { SendDocsComponent } from './send-docs/send-docs.component';
import { CaseLogComponent } from './task-management/case-log/case-log.component';
import { ProcessViewComponent } from './task-management/process-view/process-view.component';

@NgModule({
  declarations: [
    AppComponent,
    TaskManagementComponent,
    NewCaseComponent,
    EvaluateRiskComponent,
    SendDocsComponent,
    CaseLogComponent,
    ProcessViewComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

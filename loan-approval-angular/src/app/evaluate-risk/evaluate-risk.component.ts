import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoanApplication } from 'src/model/LoanApplication';
import { LoanApplicationService } from '../services/loan-application.service';
import { CaselogikClient } from '../caselogik-client/CaselogikClient';

@Component({
  selector: 'app-evaluate-risk',
  templateUrl: './evaluate-risk.component.html',
  styleUrls: ['./evaluate-risk.component.scss']
})
export class EvaluateRiskComponent implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private loanAppService: LoanApplicationService,
    private clClient: CaselogikClient) { }

  taskId: number;
  loanApp: LoanApplication;
  taskActive = false;

  ngOnInit() {
    this.route.queryParams.subscribe(
      params => {
        this.taskId = params.taskId;
        const id = params.workDataKey;
        this.loanAppService.getLoanApplication(id, (res: LoanApplication[]) => {
          this.loanApp = res[0];
        });
      }
    );
  }

  activateTask() {
    this.clClient.activateTask(this.taskId, (status, resp) => {
      if (status === 200) { this.taskActive = true; }
    });
  }

  submit() {
    this.loanAppService.updateLoanApplication(this.loanApp, (resp) => {
      const workData = { key: this.loanApp._id, riskScore: this.loanApp.riskScore };
      this.clClient.recordTaskCompleted(this.taskId, workData, (resp2) => {
        this.router.navigate(['/task-mgmt']);
      });
    });
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluateRiskComponent } from './evaluate-risk.component';

describe('EvaluateRiskComponent', () => {
  let component: EvaluateRiskComponent;
  let fixture: ComponentFixture<EvaluateRiskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvaluateRiskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluateRiskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

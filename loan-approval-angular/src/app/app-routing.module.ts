import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TaskManagementComponent } from './task-management/task-management.component';
import { EvaluateRiskComponent } from './evaluate-risk/evaluate-risk.component';
import { SendDocsComponent } from './send-docs/send-docs.component';
import { CaseLogComponent } from './task-management/case-log/case-log.component';
import { ProcessViewComponent } from './task-management/process-view/process-view.component';


const routes: Routes = [
  { path: 'task-mgmt', component: TaskManagementComponent },
  { path: 'evaluate-risk', component: EvaluateRiskComponent },
  { path: 'send-docs', component: SendDocsComponent },
  { path: 'task-mgmt/case-log/:id', component: CaseLogComponent },
  { path: 'task-mgmt/process-view/:id', component: ProcessViewComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

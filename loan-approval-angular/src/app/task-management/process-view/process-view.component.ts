
import { Component, OnInit, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { graphviz } from 'd3-graphviz';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { CaseLog } from 'src/app/caselogik-client/Model';

// npm install d3-graphviz --save

@Component({
  selector: 'app-process-view',
  template: '<h4>{{ procId }}</h4><br><div id="graph"></div>'
})
export class ProcessViewComponent implements OnInit  {

  procId: string;

  constructor(private http: HttpClient, private route: ActivatedRoute) {}

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.procId = params.id;
      this.http.get<CaseLog>('http://localhost:8080/api/v1/processes/' + this.procId + '?visual=true')
      .subscribe(resp => {
        console.log(resp);
        graphviz('#graph').renderDot(resp.graphViz.content);
      });

    });
  }

}

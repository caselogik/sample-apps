import { Component, OnInit, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { graphviz } from 'd3-graphviz';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { CaseLog } from 'src/app/caselogik-client/Model';


// npm install d3-graphviz --save

@Component({
  selector: 'app-case-log',
  template: '<h4>Case Log</h4><br><div id="graph"></div>'
})
export class CaseLogComponent implements OnInit  {

  constructor(private http: HttpClient, private route: ActivatedRoute) {}

  ngOnInit() {

    this.route.params.subscribe(params => {
      const id = params.id;
      this.http.get<CaseLog>('http://localhost:8080/api/v1/cases/' + id + '/log?visual=true')
      .subscribe(resp => {
        console.log(resp);
        graphviz('#graph').renderDot(resp.graphViz.content);
      });

    });
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaseLogComponent } from './case-log.component';

describe('CaseLogComponent', () => {
  let component: CaseLogComponent;
  let fixture: ComponentFixture<CaseLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaseLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaseLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

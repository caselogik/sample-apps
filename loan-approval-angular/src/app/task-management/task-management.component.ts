import { Component, OnInit } from '@angular/core';
import { LoanApplication } from 'src/model/LoanApplication';
import { CaselogikClient } from '../caselogik-client/CaselogikClient';
import { Process, WfCase, TaskQueue, Task } from '../caselogik-client/Model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-task-management',
  templateUrl: './task-management.component.html',
  styleUrls: ['./task-management.component.scss']
})
export class TaskManagementComponent implements OnInit {

  showNewCaseForm = false;
  loanApp: LoanApplication;
  cases: WfCase[];
  taskQueues: TaskQueue[];
  worklistTasks: Task[];

  constructor(private clClient: CaselogikClient, private router: Router) { }

  ngOnInit() {
    this.refreshCasesTab();
  }

  tabActivated(tabNum: number) {
    if (tabNum === 2) { this.refreshTaskDistributionTab(); }
  }

  refreshCasesTab() {
    this.cases = [];
    this.loadCases();
  }

  refreshTaskDistributionTab() {
    this.loadTaskQueues();
    this.loadWorklist();
  }

  loadCases() {
    this.clClient.getCases('approve_loan', (cases: WfCase[]) => {
      this.cases = cases;
    });
  }

  loadWorklist() {
    this.clClient.getTasksInWorklist('nbiswas', (tasks: Task[]) => {
      this.worklistTasks = tasks;
    });
  }

  loadTaskQueues() {
    this.clClient.getTaskQueues('approve_loan', (taskQueues: TaskQueue[]) => {
      this.taskQueues = taskQueues;
    });
  }

  newCaseForm() {
    if (!this.showNewCaseForm) {
      this.loanApp = new LoanApplication();
      this.showNewCaseForm = true;
    } else {
      this.showNewCaseForm = false;
    }
  }

  onNewCaseSubmitted() {
    this.showNewCaseForm = false;
    this.loadCases();
  }

  showClaimTask(tq: TaskQueue) {
    return  tq.actorType === 'Human' && tq.taskCount > 0;
  }

  claimNextTask(tq: TaskQueue) {
    this.clClient.claimNextTask(tq, 'nbiswas', null, (httpStatus, resp) => {
        this.refreshTaskDistributionTab();
    });
  }

  openTask(t: Task) {
    this.router.navigateByUrl(t.executionUrl);
  }
}

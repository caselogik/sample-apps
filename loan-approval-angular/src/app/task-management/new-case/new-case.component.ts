import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LoanApplication } from 'src/model/LoanApplication';
import { LoanApplicationService } from 'src/app/services/loan-application.service';
import { CaselogikClient } from 'src/app/caselogik-client/CaselogikClient';

@Component({
  selector: 'app-new-case',
  templateUrl: './new-case.component.html',
  styleUrls: ['./new-case.component.scss']
})
export class NewCaseComponent implements OnInit {

  @Input() loanApp: LoanApplication;
  @Output() submitted = new EventEmitter<boolean>();

  constructor(private loanAppService: LoanApplicationService, private clClient: CaselogikClient) { }

  ngOnInit() {}

  submitNewLoanApp() {
    this.loanAppService.saveLoanApplication(this.loanApp, (resp) => {
      const wi = { key: resp._id, riskScore: -1 };
      this.clClient.createCase('approve_loan', wi, (httpStatus: number, resp2: any) => {
        console.log(httpStatus);
        this.submitted.emit(true);
      });
    });
  }

}

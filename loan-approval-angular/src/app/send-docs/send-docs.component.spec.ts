import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendDocsComponent } from './send-docs.component';

describe('SendDocsComponent', () => {
  let component: SendDocsComponent;
  let fixture: ComponentFixture<SendDocsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendDocsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendDocsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

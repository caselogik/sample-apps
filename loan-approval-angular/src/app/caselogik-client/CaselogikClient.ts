import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Process, WfCase, TaskQueue, Worker, Task } from './Model';
import { Injectable } from '@angular/core';

const rootUrl = 'http://localhost:8080/api/v1';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
    providedIn: 'root'
})
export class CaselogikClient {

    constructor() {}

    // --------------------------------------------------------
    getCases(processId: string, callback: (cases: WfCase[]) => void) {
      const url = '/cases?processId=' + processId;
      this.get(url, callback);
    }

    // --------------------------------------------------------
    getTaskQueues(processId: string, callback: (queues: TaskQueue[]) => void) {
      const url = '/task-queues/' + processId;
      this.get(url, callback);
    }

    // --------------------------------------------------------
    claimNextTask(taskQ: TaskQueue, workerId: string, workerAttribs: object, callback: (httpStatus: number, resp: any) => void) {
      const url = '/task-queues/' + taskQ.processId + '/' + taskQ.taskNodeId + '/claim';
      const cmd = { byWorker: this.human(workerId, workerAttribs) };
      this.put(url, cmd, callback);
    }

    // --------------------------------------------------------
    getTasksInWorklist(workerId: string, callback: (queues: Task[]) => void) {
      const url = '/worklists/' + workerId + '/tasks';
      this.get(url, callback);
    }

    // --------------------------------------------------------
    createCase(procId: string, workItem: any, callback: (httpStatus: number, resp: any) => void) {
      const url = '/cases';
      const wiJson = JSON.stringify(workItem);
      const cmd = { processId: procId, workItemJson: wiJson };
      this.put(url, cmd, callback);
    }

    // ---------------------------------------
    activateTask(taskId: number, callback: (httpStatus: number, resp: any) => void) {
      const url = '/tasks/' + taskId + '/activate';
      this.put(url, {}, callback);
    }

    // ---------------------------------------
    recordTaskCompleted(taskId: number, workData: any, callback: (httpStatus: number, resp: any) => void) {
      const url = '/tasks/' + taskId + '/record-completed';
      this.put(url, { _type: 'completed', workDataJson: JSON.stringify(workData) }, callback);
    }

    // ---------------------------------------
    private get(url: string, callback: (data: any) => void) {
        const request = new XMLHttpRequest();
        request.open('GET', rootUrl + url, true);

        // tslint:disable-next-line: only-arrow-functions
        request.onload = function() {
            const result = JSON.parse(this.response);
            callback(result);
        };

        request.send();
    }

    // ---------------------------------------
    private put(url: string, data: any, callback: (httpStatus: number, resp: any) => void) {
      const request = new XMLHttpRequest();
      request.open('PUT', rootUrl + url, true);
      request.setRequestHeader('Content-type', 'application/json; charset=utf-8');

      // tslint:disable-next-line: only-arrow-functions
      request.onload = function() {
          const result = JSON.parse(this.response);
          callback(request.status, result);
      };

      request.send(JSON.stringify(data));
    }

    // -----------------------------------------------------
    private human(workerId: string, attribs: object): Worker {
      return { id: workerId, type: 'Human', attributes: attribs };
    }

}

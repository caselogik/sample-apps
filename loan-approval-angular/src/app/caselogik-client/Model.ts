import { Graphviz } from 'd3-graphviz';

export interface Process {
    id: string;
}

export interface WfCase {
    id: number;
    key: string;
    processId: string;
    status: string;
    createdAt: Date;
}

export interface TaskQueue {
    taskNodeId: string;
    taskNodeName: string;
    processId: string;
    actorType: string;
    taskCount: number;
}

export interface Worker {
    id: string;
    type: string;
    attributes: object;
}

export interface Task {
    id: number;
    processId: string;
    taskNodeId: string;
    workDataType: string;
    workDataJson: string;
    workData: any;
    worker: Worker;
    referrer: Worker;
    status: string;
    isSystemTask: boolean;
    isBatchTask: boolean;
    isReferred: boolean;
    priority: number;
    createdAt: Date;
    executionUrl: string;
}

export interface CaseLog {
    caseId: number;
    graphViz: GraphViz;
}

export interface GraphViz {
    content: string;
    format: 'dot';
}

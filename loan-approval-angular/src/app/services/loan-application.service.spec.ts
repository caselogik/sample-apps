import { TestBed } from '@angular/core/testing';

import { LoanApplicationService } from './loan-application.service';

describe('LoanApplicationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoanApplicationService = TestBed.get(LoanApplicationService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { LoanApplication } from 'src/model/LoanApplication';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoanApplicationService {

  url = 'http://localhost:4300/api/applications';

  constructor(private http: HttpClient) { }

  saveLoanApplication(loanApp: LoanApplication, callback: (resp: any) => void ) {
    this.http.post(this.url, loanApp).subscribe( res => {
      callback(res);
    });
  }

  updateLoanApplication(loanApp: LoanApplication, callback: (resp: any) => void ) {
    this.http.put(this.url, loanApp).subscribe( res => {
      callback(res);
    });
  }

  getLoanApplication(id: string, callback: (resp: any) => void) {
    this.http.get<LoanApplication>(this.url).subscribe( res => {
      console.log(res);
      callback(res);
    });
  }
}

export class LoanApplication {
    // tslint:disable-next-line: variable-name
    _id: string;
    applicantName: string;
    applicantAge: number;
    loanAmount: number;
    riskScore: number;
    rejectionSent: boolean;
    docContent: string;
    paymentSent: boolean;
}

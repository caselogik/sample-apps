package com.test.clientapp.controller;

import org.caselogik.api.Case;
import org.caselogik.api.Task;
import org.caselogik.api.TaskQueue;
import org.caselogik.client.Client;
import org.caselogik.client.ProcessContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class TaskMgmtController {

    @Autowired
    private Client client;

    //-------------------------------------------------
    @GetMapping("/task-mgmt")
    public String showWorkMgmtConsole(Model model) {

        loadTaskMgmtModel(model);
        return "task-mgmt";
    }

    //-------------------------------------------------
    @GetMapping("/task-mgmt/claim")
    public String claimTask(@RequestParam("processId") String processId, @RequestParam("taskNodeId") String taskNodeId, Model model) {

        client.workList("nbiswas").claimTaskInQueue(processId, taskNodeId);

        loadTaskMgmtModel(model);
        return "task-mgmt";
    }

    //-------------------------------------------------
    @GetMapping("/task-mgmt/activate")
    public String activateTask(@RequestParam("taskId") String taskId, @RequestParam("fromView") String fromView) {

        return "";
    }

    //-------------------------------------------------
    private void loadTaskMgmtModel(Model model){

        ProcessContext process = client.process("approve_loan");

        List<TaskQueue> queues = process.getTaskQueues();

        model.addAttribute("taskQueues", queues);

        List<Case> cases = process.getLiveCases();
        model.addAttribute("cases", cases);

        List<Task> wlTasks = client.workList("nbiswas").getTasks();
        model.addAttribute("wlTasks", wlTasks);

    }
}

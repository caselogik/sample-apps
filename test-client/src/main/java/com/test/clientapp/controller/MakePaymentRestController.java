package com.test.clientapp.controller;


import org.caselogik.api.commands.task.ExecutionStatusUpdate;
import org.caselogik.client.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MakePaymentRestController {

    @Autowired
    Client workflowClient;

    //------------------------------------------------------------------------------------------
    @GetMapping(path = "/make-payment")
    @ResponseBody
    public ResponseEntity<ExecutionStatusUpdate> executeTask(@RequestParam Long taskId){
        System.out.println("****** Executing Task id=("+taskId+")");

        //ExecutionStatusUpdate resp = workflowClient.task(task.id).newExecutionStatusUpdate().recordTaskFailed("No reason");
        ExecutionStatusUpdate resp = workflowClient.task(taskId).newExecutionStatusUpdate().recordTaskCompleted();
        return ResponseEntity.status(HttpStatus.OK).body(resp);
    }
}

package com.test.clientapp.controller;

import com.test.clientapp.model.LoanApplication;
import com.test.clientapp.model.LoanApplicationRepo;
import loan.LoanAppWI;
import org.caselogik.api.Task;
import org.caselogik.client.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.Optional;

@Controller
public class EvaluateRiskController{

    @Autowired
    LoanApplicationRepo loanApplicationRepo;

    @Autowired
    Client workflowClient;

    //---------------------------------------------
    @GetMapping("/evaluate-risk")
    public String showUpdateForm(@RequestParam("taskId") Long taskId,
                                 @RequestParam("workDataKey") String workDataKey,
                                 HttpSession session,
                                 Model model) {
        Task task = workflowClient.task(taskId).get();
        long loanAppId = Long.parseLong(workDataKey);
        Optional<LoanApplication> loanAppOpt = loanApplicationRepo.findById(loanAppId);
        session.setAttribute("task", task);
        workflowClient.task(taskId).activate();
        model.addAttribute("loanApp", loanAppOpt.get());
        return "evaluate-risk";
    }

    //---------------------------------------------
    @PostMapping("/evaluate-risk")
    public String viewApplication(@ModelAttribute("loanApp") LoanApplication loanApp,
                                  HttpSession session,
                                  Model model) {
        loanApp.setEditMode("R");

        loanApplicationRepo.save(loanApp);

        Task task = (Task) session.getAttribute("task");
        session.removeAttribute("task");

        LoanAppWI wi = (LoanAppWI) task.workData;

        wi.isRisky = loanApp.getRiskScore() > 5 ? true : false;

        workflowClient.task(task.id).recordCompleted(wi);
        return "evaluate-risk";
    }
}

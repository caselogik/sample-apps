package com.test.clientapp.controller;

import com.test.clientapp.model.LoanApplication;
import com.test.clientapp.model.LoanApplicationRepo;
import loan.LoanAppWI;
import org.caselogik.client.Client;
import org.caselogik.client.ProcessContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class LoanApplicationController {

    @Autowired
    private Client client;

    @Autowired
    LoanApplicationRepo loanApplicationRepo;

    @GetMapping("/new-application")
    public String captureNewApplication(Model model) {
        model.addAttribute("loanApp", new LoanApplication());
        return "new-application";
    }

    @PostMapping("/new-application")
    public String viewApplication(@ModelAttribute("loanApp") LoanApplication loanApp, Model model) {
        loanApp.setEditMode("R");

        loanApplicationRepo.save(loanApp);

        LoanAppWI wi = new LoanAppWI();

        wi.setKey(Long.toString(loanApp.getId()));
        wi.isRisky = true;

        client.process("approve_loan").createCase(wi);
        model.addAttribute("loanApp", loanApp);

        return "new-application";
    }
}

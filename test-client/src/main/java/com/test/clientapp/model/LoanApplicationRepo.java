package com.test.clientapp.model;

import org.springframework.data.repository.CrudRepository;

public interface LoanApplicationRepo extends CrudRepository<LoanApplication, Long> {
}


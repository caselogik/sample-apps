package com.test.clientapp.config;

import org.caselogik.client.Caselogik;
import org.caselogik.client.Client;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import loan.LoanAppWI;

@Configuration
public class BeanConfig {

    //-------------------------------------
    @Bean
    public Client caselogikClient() {
        Client client = Caselogik.client("http://localhost:8080");
        client.enableWorkDataDeserialisationFor(LoanAppWI.class);
        return client;
    }
}
